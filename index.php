<?php
// Config
require_once "vendor/autoload.php";
require_once "config/config.php";
// Routes
require_once "routes/clients.php";
require_once "routes/category.php";
require_once "routes/plat.php";
require_once "routes/order.php";
require_once "routes/login.php";
require_once "routes/image.php";
$app->run();
