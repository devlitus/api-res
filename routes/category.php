<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;
require_once "functions/functions.php";

$app = new App($c);
$app->get("/categoria", function (Request $request, Response $response) use ($app, $db){
    $consulta = "SELECT * FROM categorias";
    $query = $db->query($consulta);
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "Error de consulta");
        $db->close();
        return $response->withJson($data, 500);
    }
    $row = [];
    while ($rows = $query->fetch_assoc()){
        $row[] =$rows;
    }
    $data = array("ok" => true, "categoria" => $row);
    $db->close();
    return $response->withJson($data, 200);
});
$app->post("/categoria", function (Request $request, Response $response) use ($app, $db){
    $strValue='';
    $dataColumn = [];
    $body = $request->getParsedBody();
    foreach ($body as $key => $value){
        $dataColumn[] = $key;
        $strValue .= "'$value', ";
    }

    $strColumn = join(", ", $dataColumn);
    $strVal = substr($strValue, 0, -2);
//    echo "INSERT INTO usuarios ($strColumn) VALUES ($strVal);";
    $consulta = "INSERT INTO categorias ($strColumn) VALUES ($strVal);";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "No se ha podido crear la nueva categoría");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Categoria creado");
    $db->close();
    return $response->withJson($data, 200);
});
$app->put("/categoria/{id}", function (Request $request, Response $response, $args) use($app, $db){
    $id = $args['id'];
    $strValue = "";
    $body = $request->getParsedBody();
    $query = $db->query("SELECT id FROM categorias WHERE id=$id;");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe este categoria ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    foreach ($body as $key => $value){
        $strValue .= $key. "=". "'$value', ";
    }
    $strVal = substr($strValue, 0, -2);
//    echo "UPDATE usuarios SET $strVal WHERE id=$id;";
    $consulta = "UPDATE categorias SET $strVal WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Categoría actualizado");
    return $response->withJson($data, 200);
});
$app->delete("/categoria/{id}", function (Request $request, Response $response, $args) use ($app, $db){
    $id = $args['id'];
    $query = $db->query("SELECT id FROM categorias WHERE id=$id;");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe esta categoría ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $consulta = "DELETE FROM categorias WHERE id=$id;";
    $result = $db->query($consulta);
    if (!$result){
        $data = array("ok" => false, "message" =>"Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Categoría Eliminado");
    $db->close();
    return $response->withJson($data, 200);
});