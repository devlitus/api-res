<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;
require_once "functions/functions.php";

$app = new App($c);
$app->get("/cliente", function (Request $request, Response $response) use ($app, $db){
    $consulta = "SELECT * FROM usuarios";
    $query = $db->query($consulta);
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "Error de consulta");
        $db->close();
        return $response->withJson($data, 500);
    }
    $fila = [];
    while ($filas = $query->fetch_assoc()){
        $fila[] =$filas;
    }
    $data = array("ok" => true, "usuario" => $fila);
    $db->close();
    return $response->withJson($data, 200);
});
$app->post("/cliente", function (Request $request, Response $response) use ($app, $db){
    $strValue='';
    $dataColumn = [];
    $body = $request->getParsedBody();
    $email = $body['email'];
    if (key_exists("password", $body)){
        $pass = array("password" => password_hash($body['password'], PASSWORD_DEFAULT, ['cost' => 8]));
        $newUsuario = array_replace($body, $pass);
        foreach ($newUsuario as $key => $value) {
            $dataColumn[] = $key;
            $strValue .= "'$value', ";
        }
    }else{
        foreach ($body as $key => $value){
            $dataColumn[] = $key;
            $strValue .= "'$value', ";
        }
    }
    $uploadedFiles = $request->getUploadedFiles();
    $container = $app->getContainer();
    $container['upload_directory'] = 'uploads/clients';
    $directory = $this->get('upload_directory');
    if (isset($uploadedFiles['imagen'])){
        $uploadedFile = $uploadedFiles['imagen'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK){
            $filename = moveUploadedFile($directory, $uploadedFile);
            $dataColumn[] = 'imagen';
            $strValue .= "'$filename', ";
        }
    }
    $strColumn = join(", ", $dataColumn);
    $strVal = substr($strValue, 0, -2);
//    echo "INSERT INTO usuarios ($strColumn) VALUES ($strVal);";
    $consulta = "INSERT INTO usuarios ($strColumn) VALUES ($strVal);";
    $query = $db->query($consulta);
    if (!$query){
        $result = $db->query("SELECT nombre FROM usuarios WHERE email='$email'");
        if ($result->num_rows > 0){
            $data = array("ok" => false, "message" => 'El email '. $email . ' ya existe');
            $db->close();
            return $response->withJson($data, 206);
        }
        if (!empty($filename)){
            unlink(__DIR__."\\uploaded\\clients\\".$filename);
        }
        $data = array("ok" => false, "message" => "No se ha podido crear el nuevo cliente");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente creado");
    $db->close();
    return $response->withJson($data, 201);
});
$app->put("/cliente/{id}", function (Request $request, Response $response, $args) use($app, $db){
    $id = $args['id'];
    $strValue = "";
    $body = $request->getParsedBody();
    $query = $db->query("SELECT id FROM usuarios WHERE id=$id;");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe este cliente ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    if (key_exists("password", $body)){
        $pass = array("password" => password_hash($body['password'], PASSWORD_DEFAULT, ['cost' => 8]));
        $newUsuario = array_replace($body, $pass);
        foreach ($newUsuario as $key => $value) {
            $strValue .= $key. "=". "'$value', ";
        }
    }else{
        foreach ($body as $key => $value){
            $strValue .= $key. "=". "'$value', ";
        }
    }
    $strVal = substr($strValue, 0, -2);
//    echo "UPDATE usuarios SET $strVal WHERE id=$id;";
    $consulta = "UPDATE usuarios SET $strVal WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente actualizado");
    $db->close();
    return $response->withJson($data, 201);
});
$app->delete("/cliente/{id}", function (Request $request, Response $response, $args) use ($app, $db){
    $id = $args['id'];
    $query = $db->query("SELECT id FROM usuarios WHERE id='$id';");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe este cliente ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $consulta = "UPDATE usuarios SET  activado=0 where id='$id'";
    $result = $db->query($consulta);
    if (!$result){
        $data = array("ok" => false, "message" =>"Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente Eliminado");
    $db->close();
    return $response->withJson($data, 200);
});
$app->post("/image/{id}", function (Request $request, Response $response, $args) use ($app, $db) {
    $strValue = '';
    $id = $args['id'];
    $query = $db->query("SELECT id FROM usuarios WHERE id='$id';");
    if (!$query->num_rows > 0){
        $data = array("ok" => false, "message" => "no existe el cliente ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $uploadedFiles = $request->getUploadedFiles();
    $container = $app->getContainer();
    $container['upload_directory'] = 'uploads/clients';
    $directory = $this->get('upload_directory');
    if (isset($uploadedFiles['imagen'])){
        $uploadedFile = $uploadedFiles['imagen'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK){
            $filename = moveUploadedFile($directory, $uploadedFile);
            $dataColumn[] = 'imagen';
            $strValue .= "'$filename'";
        }
    }
    // echo "UPDATE usuarios SET imagen=$strValue WHERE id=$id;";
    $consulta = "UPDATE usuarios SET imagen = $strValue WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente actualizado");
    $db->close();
    return $response->withJson($data, 201);
});