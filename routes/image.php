<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;
require_once "functions/functions.php";

$app->post("/imageCliente/{id}", function (Request $request, Response $response, $args) use ($app, $db) {
    $strValue = '';
    $id = $args['id'];
    $query = $db->query("SELECT id FROM usuarios WHERE id='$id';");
    if (!$query->num_rows > 0){
        $data = array("ok" => false, "message" => "no existe el cliente ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $uploadedFiles = $request->getUploadedFiles();
    $container = $app->getContainer();
    $container['upload_directory'] = 'uploads/clients';
    $directory = $this->get('upload_directory');
    if (isset($uploadedFiles['imagen'])){
        $uploadedFile = $uploadedFiles['imagen'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK){
            $filename = moveUploadedFile($directory, $uploadedFile);
            $dataColumn[] = 'imagen';
            $strValue .= "'$filename'";
        }
    }
    // echo "UPDATE usuarios SET imagen=$strValue WHERE id=$id;";
    $consulta = "UPDATE usuarios SET imagen = $strValue WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente actualizado");
    $db->close();
    return $response->withJson($data, 201);
});
$app->post("/imagePlato/{id}", function (Request $request, Response $response, $args) use ($app, $db) {
    $strValue = '';
    $id = $args['id'];
    $query = $db->query("SELECT id FROM platos WHERE id='$id';");
    if (!$query->num_rows > 0){
        $data = array("ok" => false, "message" => "no existe el cliente ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $uploadedFiles = $request->getUploadedFiles();
    $container = $app->getContainer();
    $container['upload_directory'] = 'uploads/plat';
    $directory = $this->get('upload_directory');
    if (isset($uploadedFiles['imagen'])){
        $uploadedFile = $uploadedFiles['imagen'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK){
            $filename = moveUploadedFile($directory, $uploadedFile);
            $dataColumn[] = 'imagen';
            $strValue .= "'$filename'";
        }
    }
    // echo "UPDATE usuarios SET imagen=$strValue WHERE id=$id;";
    $consulta = "UPDATE platos SET imagen = $strValue WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Cliente actualizado");
    $db->close();
    return $response->withJson($data, 201);
});