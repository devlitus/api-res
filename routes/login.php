<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;

$app = new App($c);
$app->post("/login", function (Request $request, Response $response) use ($app, $db){
    $body = $request->getParsedBody();
    $email = $body['email'];
    $pass = $body['password'];
    $consulta = "SELECT * FROM usuarios WHERE email='$email';";
    $query = $db->query($consulta);
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "Error en la consulta base de datos");
        $db->close();
        return $response->withJson($data, 500);
    }
    $row = $query->fetch_assoc();
    $has = $row['password'];
    if (!password_verify($pass, $has)){
        $data = array("ok" => false, "message" => 'Email o contraseña incorrecto');
        return $response->withJson($data, 400);
    }
    $data = array(
        "ok" => true,
        "cliente" => array(
            "nombre" => $row['nombre'],
            "email" => $row['email'],
            "activado" => $row['activado'],
            "imagen" => $row['imagen'],
            "role" => $row['role']));
    return $response->withJson($data, 200);
});
