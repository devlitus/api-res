<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;
$app = new App($c);

$app->post("/pedido", function (Request $request, Response $response) use($app, $db){
    $body = $request->getParsedBody();
    $id_usuario = $body['id_usuario'];
    $id_plato = $body['id_plato'];
    $consulta = "INSERT INTO pedidos (id_usuario, id_plato) VALUES ('$id_usuario', '$id_plato');";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la consulta");
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "El pedido creado correctamente");
    return $response->withJson($data, 201);
});