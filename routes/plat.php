<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\App;
require_once "functions/functions.php";

$app = new App($c);
$app->get("/plato", function (Request $request, Response $response) use ($app, $db){
//    $db->set_charset('utf-8');
    $consulta = "SELECT * FROM platos;";
    $query = $db->query($consulta);
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "Error de consulta");
        $db->close();
        return $response->withJson($data, 500);
    }
    $row = [];
    while ($rows = $query->fetch_assoc()){
        $row[] =$rows;
    }
    $data = array("ok" => true, "plato" => $row);
    $db->close();
    return $response->withJson($data, 200);
});
$app->post("/plato", function (Request $request, Response $response) use ($app, $db){
//    $db->set_charset('utf-8');
    $strValue='';
    $dataColumn = [];
    $body = $request->getParsedBody();
    foreach ($body as $key => $value){
        $dataColumn[] = $key;
        $strValue .= "'$value', ";
    }
    $uploadedFiles = $request->getUploadedFiles();
    $container = $app->getContainer();
    $container['upload_directory'] = 'uploads/plat';
    $directory = $this->get('upload_directory');
    if (isset($uploadedFiles['imagen'])){
        $uploadedFile = $uploadedFiles['imagen'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK){
            $filename = moveUploadedFile($directory, $uploadedFile);
            $dataColumn[] = 'imagen';
            $strValue .= "'$filename', ";
        }
    }
    $strColumn = join(", ", $dataColumn);
    $strVal = substr($strValue, 0, -2);
//    echo "INSERT INTO platos ($strColumn) VALUES ($strVal);";
    $consulta = "INSERT INTO platos ($strColumn) VALUES ($strVal);";
    $query = $db->query($consulta);
    if (!$query){
        if (empty($filename)){
            unlink(__DIR__."uploaded/clients/".$filename);
        }
        $data = array("ok" => false, "message" => "No se ha podido crear el nuevo plato");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Plato creado");
    $db->close();
    return $response->withJson($data, 200);
});
$app->put("/plato/{id}", function (Request $request, Response $response, $args) use($app, $db){
//    $db->set_charset('utf-8');
    $id = $args['id'];
    $strValue = "";
    $body = $request->getParsedBody();
    $query = $db->query("SELECT id FROM platos WHERE id=$id;");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe este plato ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    foreach ($body as $key => $value){
        $strValue .= $key. "=". "'$value', ";
    }
    $strVal = substr($strValue, 0, -2);
//    echo "UPDATE usuarios SET $strVal WHERE id=$id;";
    $consulta = "UPDATE platos SET $strVal WHERE id=$id;";
    $query = $db->query($consulta);
    if (!$query){
        $data = array("ok" => false, "message" => "Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Plato actualizado");
    $db->close();
    return $response->withJson($data, 200);
});
$app->delete("/plato/{id}", function (Request $request, Response $response, $args) use ($app, $db){
    $id = $args['id'];
    $query = $db->query("SELECT id FROM platos WHERE id=$id;");
    if (!$query->num_rows){
        $data = array("ok" => false, "message" => "no existe este platos ".$id);
        $db->close();
        return $response->withJson($data, 400);
    }
    $consulta = "UPDATE platos SET activado=0 where id=$id;";
    $result = $db->query($consulta);
    if (!$result){
        $data = array("ok" => false, "message" =>"Error en la petición");
        $db->close();
        return $response->withJson($data, 500);
    }
    $data = array("ok" => true, "message" => "Plato Eliminado");
    $db->close();
    return $response->withJson($data, 200);
});
