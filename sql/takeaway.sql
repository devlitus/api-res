/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.37-MariaDB : Database - takeaway
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`takeaway` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `takeaway`;

/*Table structure for table `categorias` */

DROP TABLE IF EXISTS `categorias`;

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(100) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `categorias` */

LOCK TABLES `categorias` WRITE;

insert  into `categorias`(`id`,`categoria`,`descripcion`) values (1,'entrante','Todos los entrantes'),(2,'primeros','Todos los primeros platos'),(3,'segundos','Todos los segundos platos'),(4,'postres','Todos los postres'),(5,'bebidas','Todas las bebidas');

UNLOCK TABLES;

/*Table structure for table `pedidos` */

DROP TABLE IF EXISTS `pedidos`;

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_plato` int(11) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_pedido` (`id_usuario`),
  KEY `fk_plato_pedido` (`id_plato`),
  CONSTRAINT `fk_plato_pedido` FOREIGN KEY (`id_plato`) REFERENCES `platos` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_usuario_pedido` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pedidos` */

LOCK TABLES `pedidos` WRITE;

UNLOCK TABLES;

/*Table structure for table `platos` */

DROP TABLE IF EXISTS `platos`;

CREATE TABLE `platos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) CHARACTER SET utf8 NOT NULL,
  `descripcion` text CHARACTER SET utf8,
  `precio` decimal(4,2) DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activado` tinyint(1) DEFAULT '1',
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY (`id`,`id_categoria`),
  KEY `fk_plato_catgoria` (`id_categoria`),
  CONSTRAINT `fk_plato_catgoria` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

/*Data for the table `platos` */

LOCK TABLES `platos` WRITE;

insert  into `platos`(`id`,`nombre`,`descripcion`,`precio`,`imagen`,`fecha`,`activado`,`id_categoria`) values (1,'Sopa de verduras','Sopa de verduras','5.26','ea736442a47385c5.jpg','2019-01-16 15:53:35',1,1),(2,'Salmon con tostada','Salmon con tostada','4.32','cac2c6e718076db1.jpg','2019-01-16 15:55:06',1,1),(3,'Ternera guarnicion','Ternera con guarnicion','10.21','6d82e0b9ef9f3279.jpg','2019-01-16 16:01:34',1,2),(4,'Corte redondo','Corte redondo ternea','15.21','0201ed75e548e3fa.jpg','2019-01-16 16:17:53',1,2),(5,'Ternera con menstra','Ternera con menstra','10.24','d3a66fe8c9e97d23.jpg','2019-01-16 16:20:21',1,3),(6,'Huevos con salsichas','Huevos con salsichas','6.41','06439e610bbf6056.jpg','2019-01-16 16:21:16',1,3),(7,'Corte de nata y cacao','Corte de nata y cacao','2.28','a373a51d37c5ae6a.jpg','2019-01-16 16:39:35',1,4),(8,'Browne chocolate','Browne de chocolate','2.28','ff3ec0856c962247.jpg','2019-01-16 16:40:14',1,4),(9,'Ensalada de maiz','Ensalada con trozoas de maiz','4.00','2c694cb434712f69.jpg','2019-01-26 09:26:37',1,1),(10,'Chuleton a la sal','Chuleton a la sal','20.00','38ec57be51e7d146.jpg','2019-01-26 09:56:34',1,3),(11,'Espaguetis','Espaguetis con sala a la Napolitana','15.00','966b8ad4b61766cd.jpg','2019-01-26 10:16:52',1,2);

UNLOCK TABLES;

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `imagen` varchar(255) DEFAULT 'default.png',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activado` varchar(11) DEFAULT 'activado',
  `role` varchar(20) DEFAULT 'USER_ROLE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

LOCK TABLES `usuarios` WRITE;

insert  into `usuarios`(`id`,`nombre`,`email`,`password`,`imagen`,`fecha`,`activado`,`role`) values (1,'Test1','test1@test1.com','$2y$10$lSImKyvjH//eBo9mMvWcrOGSmpB48WyjM/J07SStGpcklfsMd1JIi','3a0a0ee025c81eb7.png','2019-01-16 15:09:31','0','USER_ROLE'),(2,'Test 2','test2@test.com','$2y$10$MH0g204t6IBYH3/kViTaJ.QePHDay8VID3Yh1r6lDpZTYNoqDzPjq','9aafb8b57de82ccb.png','2019-01-16 15:09:58','0','USER_ROLE'),(3,'Test 3','test3@test.com','$2y$08$eTTJ2EIufAnv1/SG.WcRGuEpqD7es5VsHCVUbnVIhJZYX5B3y6ovy','88572f66b30a825e.png','2019-01-16 15:10:31','0','USER_ROLE'),(4,'Test 4','test4@test4.com','$2y$10$a4dtTXBKxlkkxtpQbABBTuVniJDD7hK69b4YdctiV.1rKgTP63hSq','default.png','2019-01-16 15:23:28','0','USER_ROLE'),(5,'test5','test5@test.com','$2y$10$nei48svvKzrWfQ3wfsF7m.Rj5nwS73Mjmz8.C4sf3rtfnXy9iPM3S','6da8d0b53343c8bf.png','2019-01-17 16:44:35','1','USER_ROLE'),(6,'test6','test6@test.com','$2y$08$OJO37nntSqXD31zb.xkK5.vR42d2WiCbtSxF6yQaH3DZMoBdyTTke','52007cad23b735d6.png','2019-01-17 16:45:01','1','USER_ROLE'),(7,'test7','test7@test.com','$2y$08$IVroSU5uzQS62eL.gtCkbedTAbGM/q1izjWpErH03ZWq/NIgxdkyW','57ae0e8e830e684a.png','2019-01-17 16:46:53','1','USER_ROLE'),(9,'test8','test8@test.com','$2y$08$WgcsZA4e5CaHLaoX9e/o3OOFQKciylzcbSmpdWLFOB2P1icg63Bse','c5cc8623d7a253fc.png','2019-01-17 16:51:56','1','ADMIN_ROLE'),(10,'test9','test9@test9.com','$2y$08$ls.AiAeWEjrCW/.8tzist.qHxzB.hA7V57nl6HilHY/Fo3UiLmPPm','default.png','2019-01-18 08:38:23','1','ADMIN_ROLE'),(11,'test10','test10@test.com','$2y$08$ckX12rJfgq3NgrYI47DLc.geV27WuS5z5rhkZQ.iPlF0h.ARHgyAy','6768750d8bd76720.png','2019-01-18 09:37:41','1','USER_ROLE'),(12,'test11','test11@test.com','$2y$08$sad1HXM10EACFcDDned.q.vFUI2QTucspIQOh4E727rZ3nIAyt/tS','default.png','2019-01-18 09:42:08','1','USER_ROLE'),(15,'test12','test12@test.com','$2y$08$oVYBNheWUUStW0e02BOkseGaptzvqWync.ZtV0HkWUiGMlcf8x2LG','default.png','2019-01-18 11:21:58','1','ADMIN_ROLE'),(16,'test13','test13@test.com','$2y$08$9rEjnmeNqEr.yVm/2b/rI.VtXYmPbt7QRPs3Xg5ZEtzgenFRjSRpe','default.png','2019-01-18 11:29:55','1','USER_ROLE'),(17,'test14','test14@test.com','$2y$08$gb7awbgOn.oI0J4jrs./KeQMfFEk7oBexgvfwRWeaBO9KhkvBQ61W','default.png','2019-01-18 11:30:34','1','USER_ROLE'),(18,'Test15','test15@test.com','$2y$08$Q0opVbVtp2evx45BisTrZe.JuVoGZTD/pS6bT05I23wFTCkgRs166','3ce361705053db18.png','2019-01-22 11:16:34','0','USER_ROLE'),(23,'Test16','test16@test.com','$2y$08$LTHIAfNYz6b3NIhhyQWU3.XaHJWMSB7Yk0iDOHTwcdEN0.BADxklq','9f7414ce38ac08d0.png','2019-01-22 11:33:37','0','USER_ROLE'),(24,'Test17','test17@test.com','$2y$08$bg4kDOLJTCX0ypvchtb/HOIw9UIu/P4v3p.lEM6AHmACHQBEV7hty','872f7ee8997fbf42.png','2019-01-22 11:34:18','0','USER_ROLE'),(25,'Test18','test18@test.com','$2y$08$K0cckvDp4.UHKGAOSlpA4O4DqL8Xv0mIv3GDd6q0M95kw30UAXsy6','5c4fb600858e76f6.png','2019-01-24 12:25:45','true','USER_ROLE');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
